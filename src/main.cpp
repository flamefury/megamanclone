#include <SFML/Graphics.hpp>
#include <boost/assign/std/vector.hpp>
#include "Global.h"
#include "Level.h"
#include "Player.h"
#include <vector>
#include <iostream>
using namespace boost::assign;

Level createShadowmanStage()
{
    sf::Image tilemap;
    tilemap.loadFromFile("img/mm3_8boss_shadowman.png");
    std::vector< std::vector<int> > background;

    background.resize(16);
    for (int i=0; i<16; i++)
    {
        background[i] += 114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114,114;
    }

    std::vector< std::vector<int> > foreground;
    foreground.resize(16);
    foreground[0] +=  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
    foreground[1] +=  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
    foreground[2] +=  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
    foreground[3] +=  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
    foreground[4] +=  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
    foreground[5] +=  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  53,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
    foreground[6] +=  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  53,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
    foreground[7] +=  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0  ,53,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
    foreground[8] +=  0,0,0,0,0,0,0,0,0,0,0,0,0,0,106,53,107,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,112,113;
    foreground[9] +=  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  53,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,38;
    foreground[10] += 0,0,0,0,0,0,0,4  ,0,0,0,0,4  ,0,0,0,0,0,0,0,0,106,4,4,107,0,0,0,0,106,4,107,0,0,0,0,0,0,0,38;
    foreground[11] += 0,0,0,0,0,0,0,106,4,4,4,4,107,0,0,0,0,0,0,0,0,0,0,0,0,148,148,148,148,0,0,0,0,0,0,0,0,0,0,38;
    foreground[12] += 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,142,144,142,144,0,0,0,0,0,0,0,0,0,0,38;
    foreground[13] += 0,0,1,2,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,135,97,98,134,0,0,0,0,0,0,0,1,2,1,2;
    foreground[14] += 0,0,9,10,9,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,135,117,118,134,0,0,0,0,0,0,0,36,37,36,37;
    foreground[15] += 0,0,25,26,25,26,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,135,139,141,134,0,0,0,0,0,0,0,36,37,36,37;

    std::vector< std::vector< std::vector<int> > > layers;
    layers += background,foreground;
    return Level(tilemap,layers,16,16,true);
}

Player createMegaMan()
{
    sf::Image spritesheet;
    spritesheet.loadFromFile("img/MegaMan.png");
    return Player(spritesheet,32,32,true);
}
int main()
{
    // Some multiples of 256 by 240 pixels is the screen size. 16 by 15 tiles.
    sf::RenderWindow window(sf::VideoMode(256*SCALE, 240*SCALE), "Mega Man Clone");
    window.setKeyRepeatEnabled(true);
    window.setFramerateLimit(30);

    Level shadowmanStg = createShadowmanStage();
    shadowmanStg.setTileTypeForTexture(TILE_LADDER,53);
    shadowmanStg.setTileTypeForTexture(TILE_SPIKE,147);
    shadowmanStg.setTileTypeForTexture(TILE_SPIKE,148);
    shadowmanStg.setTileTypeForTexture(TILE_DOOR,38);

    Player MegaMan = createMegaMan();
    PlayerStatus newStatus=PLAYER_IDLE;
    // List of gameobject positions which MegaMan can collide with.
    std::vector<std::shared_ptr<SolidObject> > collidables;
    while (window.isOpen())
    {
        bool leftCollision=MegaMan.checkCollision(collidables,LEFT);
        bool rightCollision=MegaMan.checkCollision(collidables,RIGHT);
        bool bottomCollision=MegaMan.checkCollision(collidables,DOWN);
        bool topCollision=MegaMan.checkCollision(collidables,UP);

        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed
                    || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            {
                window.close();
            }
        }
        // Handle jump button press.
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
        {
            if (!topCollision && MegaMan.getStatus()!=PLAYER_FALLING)
                newStatus=PLAYER_RISING;
            else if (topCollision)
                newStatus=PLAYER_FALLING;
        }
        else if (MegaMan.getStatus()==PLAYER_RISING)
        {
            newStatus=PLAYER_FALLING;
        }
        // Jumping / gravity.
        if (newStatus==PLAYER_FALLING && !bottomCollision)
        {
            MegaMan.jump(SCALE*JUMP_UNIT);
        }
        else if (newStatus==PLAYER_RISING && !topCollision)
        {
            if (!MegaMan.jump(-SCALE*JUMP_UNIT))
                newStatus=PLAYER_FALLING;
        }
        else if (bottomCollision)
        {
            MegaMan.resolveCollision(DOWN);
            newStatus=PLAYER_IDLE;
        }
        // Left/Right keys for PLAYER_RUNNING.
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            bool isLeft=true;
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            {
                MegaMan.setLeft(isLeft);
                if (!leftCollision)
                {
                    shadowmanStg.horizontalScroll(isLeft);
                    // Mega should always be at centre of screen.
                    MegaMan.relocate(window.getSize().x/2,MegaMan.getPosition().y);
                }
            }
            else
            {
                isLeft=false;
                MegaMan.setLeft(isLeft);
                if (!rightCollision)
                {
                    shadowmanStg.horizontalScroll(isLeft);
                    // Mega should always be at centre of screen.
                    MegaMan.relocate(window.getSize().x/2,MegaMan.getPosition().y);
                }
            }


            if (newStatus!=PLAYER_RISING)
            {
                if (!bottomCollision)
                    newStatus=PLAYER_FALLING;
                else if (bottomCollision)
                    newStatus=PLAYER_RUNNING;
            }

        }
        MegaMan.setStatus(newStatus);
        // Update collidable list.
        collidables.clear();
        std::vector<Tile> levelCollidables = shadowmanStg.getTilesOnLayer(1);
        for (auto it=levelCollidables.begin(); it!=levelCollidables.end(); it++)
        {
            collidables.push_back(std::unique_ptr<SolidObject>(new Tile(*it)));
        }

        window.clear();
        MegaMan.update();
        window.draw(shadowmanStg);
        window.draw(MegaMan);
        window.display();
    }

    return 0;
}
