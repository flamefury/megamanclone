#include "Player.h"
#include "Global.h"
#include <iostream>
#include <memory>

Player::Player(sf::Image img,int tileWidth,int tileLength,bool hasBorder)
{
    tile_width=tileWidth;
    tile_length=tileLength;
    status=PLAYER_IDLE;
    activeFrame=7;
    position = sf::Vector2f(256,300);
    point = sf::Vector2f(16*SCALE,32*SCALE);
    maxJumpHeight=50*SCALE;
    jumpCounter=0;
    updateClock=sf::Clock();

    hitbox = sf::Rect<int>(7*SCALE,14*SCALE,16*SCALE,18*SCALE);

    int numColTilemap = img.getSize().x / tile_width;
    int numRowTilemap = img.getSize().y / tile_length;

    int counter_x=1;
    int counter_y=1;

    for (int i=0; i<numRowTilemap; i++)
    {
        for (int j=0; j<numColTilemap; j++)
        {
            sf::Texture t;
            if (hasBorder)
                t.loadFromImage(img,sf::IntRect(tile_width*j+counter_x,tile_length*i+counter_y,tile_width,tile_length));
            else
                t.loadFromImage(img,sf::IntRect(tile_width*j,tile_length*i,tile_width,tile_length));
            counter_x++;
            tileTextures.push_back(t);
        }
        counter_x=1;
        counter_y++;
    }
}
void Player::setStatus(PlayerStatus ps)
{
    previousStatus=status;
    status=ps;
}

void Player::relocate(int x, int y)
{
    position.x=x;
    position.y=y;
}
bool Player::jump(int y)
{
    position = sf::Vector2f(position.x,position.y);
    bool rtn=true;
    if (jumpCounter-y <= maxJumpHeight)
    {
        jumpCounter-=y;
        position = sf::Vector2f(position.x,position.y+y);
    }
    else
    {
        status=PLAYER_FALLING;
        rtn=false;
    }
    //Relocate the hitbox.
    float hx = position.x - point.x;
    float hy = position.y - point.y;
    hitbox = sf::Rect<int>(hx+SCALE*7,hy+SCALE*12,SCALE*16,SCALE*18);
    return rtn;
}
// TODO: Stop using magic numbers and replace with parsed XML data or something equally easy to edit.
void Player::update()
{
    if (status==PLAYER_IDLE)
    {
        activeFrame=7;
        jumpCounter=0;
    }
    else if (status==PLAYER_RISING)
    {
        if (previousStatus==PLAYER_IDLE || previousStatus==PLAYER_RUNNING)
            activeFrame=20;
        else if (activeFrame==24) {} //end animation frame, ignore.
        else
        {
            if (updateClock.getElapsedTime().asMilliseconds()>PLAYER_UPDATE_RATE)
            {
                activeFrame++;
                updateClock.restart();
            }
        }
    }
    else if (status==PLAYER_FALLING)
    {
        if ((previousStatus==PLAYER_RISING || previousStatus==PLAYER_FALLING) && activeFrame!=21)
        {
            if (updateClock.getElapsedTime().asMilliseconds()>PLAYER_UPDATE_RATE)
            {
                activeFrame--;
                updateClock.restart();
            }
        }
        else
            activeFrame=21;
    }
    else if (status==PLAYER_RUNNING)
    {
        jumpCounter=0;

        if (previousStatus!=PLAYER_RUNNING)
            activeFrame=10;
        else
        {
            if (updateClock.getElapsedTime().asMilliseconds()>PLAYER_UPDATE_RATE)
            {
                if (activeFrame==19)
                    activeFrame=10;
                else
                {
                    activeFrame++;
                    updateClock.restart();
                }
            }
        }
    }
}
void Player::draw(sf::RenderTarget& rt, sf::RenderStates rs) const
{
    sf::Sprite s;
    s.setTexture(tileTextures[activeFrame]);
    if (isLeft)
    {
        s.setOrigin(32,0);
        s.scale(-SCALE,SCALE);
    }
    else
    {
        s.scale(SCALE,SCALE);
    }
    // point on sprite should be where position is set.
    float x = position.x - point.x;
    float y = position.y - point.y;
    s.setPosition(x,y);
    // relocate the hitbox.
    sf::RectangleShape df = sf::RectangleShape(sf::Vector2f(hitbox.width,hitbox.height));
    df.setPosition(x+SCALE*7,y+SCALE*12);
    //rt.draw(df);
    rt.draw(s);
}

bool Player::checkCollision(std::vector<std::shared_ptr<SolidObject> > const &collidables, Direction direction)
{
    collidedObjects[direction]=NULL;

    sf::Vector2f newPosition;
    if (direction==LEFT)
        newPosition=sf::Vector2f(position.x-(SCALE*MOVE_UNIT),position.y);
    else if (direction==RIGHT)
        newPosition=sf::Vector2f(position.x+(SCALE*MOVE_UNIT),position.y);
    else if (direction==UP)
        newPosition = sf::Vector2f(position.x,position.y-(SCALE*MOVE_UNIT));
    else if (direction==DOWN)
        newPosition = sf::Vector2f(position.x,position.y+(SCALE*MOVE_UNIT));

    float hx = newPosition.x - point.x;
    float hy = newPosition.y - point.y;
    sf::FloatRect newHitbox = sf::FloatRect(hx+SCALE*7,hy+SCALE*12,SCALE*16,SCALE*18);

    for (auto it=collidables.begin(); it!=collidables.end(); it++)
    {
        sf::FloatRect objectBounds = (*it)->getGlobalBounds();

        if (newHitbox.intersects(objectBounds))
        {
            collidedObjects[direction] = *it;
            return true;
        }
    }
    return false;
}

sf::Vector2f Player::getPosition() const
{
    return position;
}

void Player::resolveCollision(Direction direction)
{
    if (collidedObjects[direction]==NULL)
        return;

    ObjectType type = collidedObjects[direction]->getType();
    sf::FloatRect objectBounds = collidedObjects[direction]->getGlobalBounds();
    onLadder=false;

    if (type==TILE_SPIKE)
    {
        std::cout << "SPIKES = DEATH" << std::endl;
    }
    else if (type==TILE_LADDER)
    {
        std::cout << "LADDER = CLIMB" << std::endl;
        onLadder=true;
    }
    else if (type==TILE_DOOR)
    {

    }
    else if (type==TILE_NORMAL)
    {
        if (direction==LEFT)
        {
            position.x=objectBounds.left+objectBounds.width + SCALE*8 + 2;
        }
        else if (direction==RIGHT)
        {
            position.x=objectBounds.left-SCALE*8;
        }
        else if (direction==DOWN)
        {
            position.y=objectBounds.top;
        }
        else if (direction==UP)
        {
            if (position.y < objectBounds.top+objectBounds.height)
                position.y = objectBounds.top+objectBounds.height+SCALE*12+2;
        }
    }
}
