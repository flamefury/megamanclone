#include "Tile.h"
#include "Global.h"
Tile::Tile(const Tile& copy_from)
{
    texture=copy_from.getTexture();
    position=copy_from.getPosition();
    textureValue=copy_from.getTextureValue();
    type=copy_from.getType();
}

Tile::Tile(sf::Texture t,sf::Vector2f p, int textureCoordinate)
{
    texture=t;
    position=p;
    type=TILE_NORMAL;
    textureValue=textureCoordinate;
}
Tile::Tile(sf::Texture t,sf::Vector2f p,ObjectType tt)
{
    texture=t;
    position=p;
    type=tt;
    textureValue=0;
}

void Tile::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    sf::Sprite s;
    s.setTexture(texture);
    s.setPosition(position);
    s.setScale(SCALE,SCALE);
    target.draw(s);
}

void Tile::move (float x, float y)
{
    position.x+=x;
    position.y+=y;
}

sf::FloatRect Tile::getGlobalBounds()
{
    sf::Sprite s;
    s.setTexture(texture);
    s.setPosition(position);
    s.setScale(SCALE,SCALE);
    return s.getGlobalBounds();
}
