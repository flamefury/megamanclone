#include "Level.h"
#include "Global.h"
#include <iostream>
#include "Tile.h"

Level::Level(sf::Image img, std::vector< std::vector< std::vector<int> > > m, int tileWidth, int tileLength,bool hasBorder)
{
    mapvalues=m;
    tile_width=tileWidth;
    tile_length=tileLength;
    int numColTilemap = img.getSize().x / tile_width;
    int numRowTilemap = img.getSize().y / tile_length;

    // Set the tile set.
    int counter_x=1;
    int counter_y=1;

    for (int i=0; i<numRowTilemap; i++)
    {
        for (int j=0; j<numColTilemap; j++)
        {
            sf::Texture t;
            if (hasBorder)
                t.loadFromImage(img,sf::IntRect(tile_width*j+counter_x,tile_length*i+counter_y,tile_width,tile_length));
            else
                t.loadFromImage(img,sf::IntRect(tile_width*j,tile_length*i,tile_width,tile_length));
            counter_x++;
            tileTextures.push_back(t);
        }
        counter_x=1;
        counter_y++;
    }


    // The fun part. Create all the tile objects.
    int current_x,current_y;
    // LAYER
    for (unsigned int l=0; l<mapvalues.size(); l++)
    {
        current_y=0;
        tiles.insert(std::pair<int,std::vector<Tile> >(l,std::vector<Tile>()));
        // ROW
        for (unsigned int y=1; y<mapvalues[l].size(); y++)
        {
            current_x=0;
            // COLUMN
            for (unsigned int x=1; x<mapvalues[l][y].size(); x++)
            {
                int tileCoordinate = mapvalues[l][y][x];
                if (tileCoordinate!=0)
                {
                    Tile t=Tile(sf::Texture(tileTextures[tileCoordinate-1]),sf::Vector2f(current_x,current_y),tileCoordinate-1);
                    tiles[l].push_back(t);
                }

                current_x+=tile_width*SCALE;
            }
            current_y+=tile_length*SCALE;
        }
    }
}
void Level::setTileTypeForTexture(ObjectType type, int textureValue)
{
    // Add to mapping.
    textureValue--; // To convert to array form.
    tileTypeMapping.insert(std::pair<ObjectType,int>(type,textureValue));
    // Update type of every tile in tiles map which matches value.
    for (unsigned int layer=0; layer<tiles.size(); layer++)
    {
        for (unsigned int i=0; i<tiles[layer].size(); i++)
        {
            if (tiles[layer][i].getTextureValue() == textureValue)
            {
                tiles[layer][i].setType(type);
            }
        }
    }
}
void Level::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    for (unsigned int layer=0; layer<tiles.size(); layer++)
    {
        std::vector<Tile> tilesInLayer=tiles.find(layer)->second;
        for (unsigned int i=0; i<tilesInLayer.size(); i++)
        {
            sf::FloatRect tile=tilesInLayer[i].getGlobalBounds();
            sf::FloatRect window = sf::FloatRect(0,0,target.getSize().x,target.getSize().y);
            if (window.intersects(tile))
                target.draw(tilesInLayer[i]);
        }
    }
}

void Level::horizontalScroll(bool isLeft)
{
    for (unsigned int layer=0; layer<tiles.size(); layer++)
    {
        for (unsigned int i=0; i<tiles[layer].size(); i++)
        {
            //Update positions of tiles
            if (isLeft)
                tiles[layer][i].move((MOVE_UNIT*SCALE),0);
            else
                tiles[layer][i].move(-(MOVE_UNIT*SCALE),0);
        }
    }
}

std::vector<Tile> Level::getTilesOnLayer(int layer)
{
    return tiles.find(layer)->second;
}
