#ifndef _LEVEL_H_
#define _LEVEL_H_

#include "TileMap.h"
#include "Tile.h"
#include <vector>
/* A platform screen.
 */
 class Level : public TileMap
 {
     public:
        Level(sf::Image,std::vector< std::vector< std::vector<int> > >,int,int,bool);
        virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
        void horizontalScroll(bool);
        std::vector<Tile> getTilesOnLayer(int);
        void setTileTypeForTexture(ObjectType,int);
     protected:
        std::vector<std::vector< std::vector<int> > > mapvalues;
        std::map<int,std::vector<Tile> > tiles;
        std::map<ObjectType,int> tileTypeMapping;
 };
#endif
