#ifndef _SOLIDOBJECT_H
#define _SOLIDOBJECT_H

#include <SFML/Graphics.hpp>
#include <iostream>

enum ObjectType
{
    TILE_NORMAL,
    TILE_SPIKE,
    TILE_DOOR,
    TILE_LADDER
};

class SolidObject : public sf::Drawable
{
    public:
        virtual void draw(sf::RenderTarget&, sf::RenderStates) const {}
        virtual ObjectType getType() const {return type;}
        virtual sf::FloatRect getGlobalBounds() {return sf::FloatRect();}
    protected:
        ObjectType type;
};

#endif
