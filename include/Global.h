#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#define SCALE 2
#define SCREEN_TILE_WIDTH 16
#define SCREEN_TILE_LENGTH 15
#define MOVE_UNIT 3
#define JUMP_UNIT 4

#endif
