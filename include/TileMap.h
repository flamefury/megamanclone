#ifndef _TILEMAP_H_
#define _TILEMAP_H_

#include <vector>
#include <SFML/Graphics.hpp>

 class TileMap : public sf::Drawable
 {
     protected:
        int tile_width,tile_length;
        std::vector<sf::Texture> tileTextures;
 };
#endif
