#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "TileMap.h"
#include "SolidObject.h"
#include <vector>
#include <memory>

/* Player character.
 * Needs: Animation set, hitbox, reponses to actions.
 */
#define PLAYER_UPDATE_RATE 50
 enum PlayerStatus
 {
     PLAYER_IDLE,
     PLAYER_RISING,
     PLAYER_FALLING,
     PLAYER_RUNNING,
     PLAYER_CLIMBING,
     PLAYER_HIT
 };

enum Direction
{
    UP,
    DOWN,
    RIGHT,
    LEFT
};

 class Player : public TileMap
 {
     public:
        Player(sf::Image,int,int,bool);
        PlayerStatus getStatus() const {return status;};
        bool jump(int);
        void relocate(int,int);
        void setStatus(PlayerStatus);
        void setLeft(bool val) {isLeft=val;}
        //void setOnLadder(bool val) {onLadder=val;}
        //bool isOnLadder() const {return onLadder;}
        void update();
        virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
        bool checkCollision(const std::vector<std::shared_ptr<SolidObject> >&,Direction);
        void resolveCollision(Direction); // TODO: Return game state once I make that.
        sf::Vector2f getPosition() const;
     protected:
        sf::Vector2f position;
        sf::Vector2f point; // Hotspot on sprite.
        int activeFrame;
        PlayerStatus previousStatus;
        PlayerStatus status;
        bool isLeft;
        bool onLadder;
        int maxJumpHeight;
        int jumpCounter;
        sf::Clock updateClock;
        sf::Rect<int> hitbox;
        std::map<Direction,std::shared_ptr<SolidObject>> collidedObjects;
 };
#endif
