#ifndef TILE_H
#define TILE_H

#include "SolidObject.h"
#include <SFML/Graphics.hpp>

class Tile : public SolidObject
{
    public:
        Tile(const Tile&);
        Tile(sf::Texture t,sf::Vector2f p,int);
        Tile(sf::Texture,sf::Vector2f,ObjectType);

        virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
        sf::FloatRect getGlobalBounds();
        void move(float x,float y);

        void setType(ObjectType t) {type=t;}

        int getTextureValue() const {return textureValue;}
        sf::Texture getTexture() const {return texture;}
        sf::Vector2f getPosition() const {return position;}

    protected:
        sf::Texture texture;
        sf::Vector2f position;
        int textureValue;
};

#endif // TILE_H
